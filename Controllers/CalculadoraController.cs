﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppi.Models;

namespace WebAppi.Controllers
{
    [Route("api/calculadora")]
    [ApiController]
    public class CalculadoraController : ControllerBase
    {
        [HttpGet]
        [Route("sumar")]
        public ActionResult<Calculadora> Sumar(int numero1, int numero2)
        {
            Calculadora calculadora = new Calculadora(numero1, numero2);
            calculadora.sumar();

            return calculadora;
        }
    }
}

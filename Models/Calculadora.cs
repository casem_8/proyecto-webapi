﻿namespace WebAppi.Models
{
    public class Calculadora
    {
        public int numero1 { get; set; }
        public int numero2 { get; set; }
        public int resultado { get; set; }

        public Calculadora ()
        {

        }

        public Calculadora(int numero1, int numero2)
        {
            this.numero1 = numero1;
            this.numero2 = numero2;
        }

        // Metodo sumar
        public void sumar()
        {
            resultado = numero1 + numero2;
        }
       
    }
}
